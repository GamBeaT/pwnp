<?php namespace App\Form;

Class CreditCardData
{

    private $cardNumber;
    private $CvvNumber;
    private $cardType;

    public function __construct($cardNumber, $CvvNumber, $cardType)
    {
        $this->cardNumber = $cardNumber;
        $this->CvvNumber = $CvvNumber;
        $this->cardType = $cardType;
    }

    public function getcardNumber()
    {
        return $this->cardNumber;
    }

    public function getCvvNumber()
    {
        return $this->CvvNumber;
    }

    public function getCardType()
    {
        return $this->cardType;
    }

}
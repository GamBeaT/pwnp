<?php

namespace App\Form;

use App\Validation\CreditCard\NumberValidation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\CardScheme;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class CreditCardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('CardNumber', IntegerType::class, [
                'constraints' => [
                    new CardScheme(['schemes'=> ['VISA'],'groups' => 'VI', 'message' => "Your credit card number is invalid."]),
                    new CardScheme(['schemes'=> ['MASTERCARD'],'groups' => 'MS', 'message' => "Your credit card number is invalid."]),
                    new CardScheme(['schemes'=> ['AMEX'],'groups' => 'AE', 'message' => "Your credit card number is invalid."]),
                ]
            ])
            ->add('CvvNumber', NumberType::class, [
                'invalid_message' => 'Cvv must be a number',
                'constraints' => [
                    new Length(['min' => 3, 'max' => 3, 'groups' => ['VI','MS','AE']])
                ]
            ])
            ->add('CardType', ChoiceType::class, [
                'choices' => [
                    'MasterCard' => 'MS',
                    'Visa' => 'VI',
                    'AmericanExpress' => 'AE',
                ]
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CreditCardData::class,
            'empty_data' => function (FormInterface $form) {
                return (new CreditCardData(
                    $form->get('CardNumber')->getData(),
                    $form->get('CvvNumber')->getData(),
                    $form->get('CardType')->getData()
                ));
            },
            'csrf_protection' => false,
            'validation_groups' => function (FormInterface $form) {
                $data = $form->getData();
                return ['default', $data->getCardType()];
            },
        ]);
    }
}

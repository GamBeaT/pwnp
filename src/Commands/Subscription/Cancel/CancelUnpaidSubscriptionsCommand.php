<?php namespace App\Commands\Subscription\Cancel;

class CancelUnpaidSubscriptionsCommand
{

    protected $daysOfInactivity;

    public function __construct(int $days)
    {
        $this->daysOfInactivity = $days;
    }

    public function getDaysOfInactivity()
    {
        return $this->daysOfInactivity;
    }

}
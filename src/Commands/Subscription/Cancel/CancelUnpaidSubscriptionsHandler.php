<?php namespace App\Commands\Subscription\Cancel;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CancelUnpaidSubscriptionsHandler implements MessageHandlerInterface
{

    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function handle(CancelUnpaidSubscriptionsCommand $command)
    {
        $daysOfInactivity = 30 + $command->getDaysOfInactivity();
        $sql = "UPDATE subscription 
                    SET status = 'inactive', updated_at = Now()
                    WHERE status = 'active' and TIMESTAMPDIFF(DAY,subscription.started_at,now()) > 30
                    AND NOT exists (
                        SELECT * FROM subscription_payment 
                        WHERE subscription.id = subscription_payment.subscription_id 
                        AND DATE(subscription_payment.date) 
                            BETWEEN DATE_SUB(subscription.started_at, INTERVAL 30 DAY) 
                            AND DATE_SUB(subscription.started_at, INTERVAL :days DAY))";
        $connection = $this->entityManager->getConnection();
        $statement = $connection->prepare($sql);
        $statement->execute(['days' => $daysOfInactivity]);
    }

    public function __invoke(CancelUnpaidSubscriptionsCommand $command)
    {
        $this->handle($command);
    }

}
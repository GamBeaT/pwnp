<?php namespace App\Commands\Subscription\Notify;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Mime\Email;

class NotifyUserHandler implements MessageHandlerInterface
{

    protected $mailer;
    protected $entityManager;

    public function __construct(MailerInterface $mailer, EntityManagerInterface $entityManager)
    {
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
    }

    public function handle(NotifyUserCommand $command)
    {
        // fetch user
        // fetch subscription

        $email = (new Email())
            ->from('subscription@example.com')
            ->to('you@user.com')
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject('Subscription activated')
            ->text('Your payment was successful! Your subscription is now active');

        //$this->mailer->send($email);
    }

    public function __invoke(NotifyUserCommand $command)
    {
        $this->handle($command);
    }

}
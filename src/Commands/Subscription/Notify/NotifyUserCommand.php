<?php namespace App\Commands\Subscription\Notify;

class NotifyUserCommand
{

    protected $id;
    protected $subscription;

    public function __construct($id, $subscription)
    {
        $this->id = $id;
        $this->subscription = $subscription;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSubscription()
    {
        return $this->subscription;
    }

}
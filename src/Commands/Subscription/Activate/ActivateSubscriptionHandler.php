<?php namespace App\Commands\Subscription\Activate;

use App\Commands\Subscription\Notify\NotifyUserCommand;
use App\Entity\Subscription;
use App\Entity\SubscriptionPayment;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class ActivateSubscriptionHandler implements MessageHandlerInterface
{

    protected $bus;
    protected $entityManager;

    public function __construct(MessageBusInterface $bus, EntityManagerInterface $entityManager)
    {
        $this->bus = $bus;
        $this->entityManager = $entityManager;
    }

    public function handle(ActivateSubscriptionCommand $command)
    {
        $subscription = $this->entityManager->getRepository(Subscription::class)->find($command->getId());
        if (!$subscription) {
            throw new EntityNotFoundException("Could not find subscription");
        }
        $subscription->setStatus("active");
        $subscription->setStartedAt(new \DateTime());
        $subscription->setUpdatedAt(new \DateTime());
        $subscriptionPayment = new SubscriptionPayment();
        $subscriptionPayment->setSubscriptionId($command->getId());
        $subscriptionPayment->setChargedAmount(3600);
        $subscriptionPayment->setDate(new \DateTime());
        $subscriptionPayment->setCreatedAt(new \DateTime());
        $this->entityManager->persist($subscriptionPayment);
        $this->entityManager->flush();
        $this->bus->dispatch(new NotifyUserCommand($subscription->getUserId(), $subscription->getId()));
    }

    public function __invoke(ActivateSubscriptionCommand $command)
    {
        $this->handle($command);
    }

}
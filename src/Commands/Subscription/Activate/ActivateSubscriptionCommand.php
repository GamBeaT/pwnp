<?php namespace App\Commands\Subscription\Activate;

class ActivateSubscriptionCommand
{

    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

}
<?php

namespace App\Controller;

use App\Commands\Subscription\Activate\ActivateSubscriptionCommand;
use App\Commands\Subscription\Cancel\CancelUnpaidSubscriptionsCommand;
use App\Form\CreditCardType;
use App\Repository\SubscriptionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/subscriptions", name="subscription_")
 */

class SubscriptionController extends AbstractController
{
    /**
     * @Route("/list", name="list")
     */
    public function index(SubscriptionRepository $repository)
    {
        $subscriptions = $repository->findBy(['user_id' => 1]);
        return $this->render('subscription/index.html.twig', [
            'subscriptions' => $subscriptions
        ]);
    }

    /**
     * @Route("/pay/{id}", name="pay")
     */
    public function pay(MessageBusInterface $bus, Request $request, int $id)
    {
        $form = $this->createForm(CreditCardType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bus->dispatch(new ActivateSubscriptionCommand($id));
            $this->addFlash(
                'message', 'Activated subscription'
            );
            return $this->redirectToRoute('subscription_list');
        }

        return $this->render('subscription/pay.html.twig', [
            'form' => $form->createView(),
            'errors' => $form->getErrors(true),
            'id' => $id
        ]);
    }

}

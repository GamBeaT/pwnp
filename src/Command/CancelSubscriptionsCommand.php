<?php

namespace App\Command;

use App\Commands\Subscription\Cancel\CancelUnpaidSubscriptionsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class CancelSubscriptionsCommand extends Command
{
    protected static $defaultName = 'app:cancel-subscriptions';
    protected $bus;

    public function __construct(MessageBusInterface $bus)
    {
        parent::__construct();
        $this->bus = $bus;
    }

    protected function configure()
    {
        $this
            ->setDescription('Command cancels subscriptions that are not paid within 7 days from expiry date')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Cancelling subscriptions that have not been paid within 7 days from expiry date');
        try {
            $this->bus->dispatch(new CancelUnpaidSubscriptionsCommand(7));
            $output->writeln('Subscriptions cancelled!');
            return Command::SUCCESS;
        } catch (\Exception $e) {
            $output->writeln('Operation failed!');
            return Command::FAILURE;
        }

    }
}
